#! /usr/bin/env bash
# ./apply.sh
# Used to apply the provided preset onto a server
GMOD_ROOT="/home/container/garrysmod/"

# ChangeDir to preset root
SELF="$0"
PRESET_ROOT="$(realpath "$SELF")"
PRESET_ROOT="$(dirname "$PRESET_ROOT")"
cd "$PRESET_ROOT"

if [[ ! -d ./preset ]]; then
    echo "Cannot apply empty preset!"
    exit 1
fi

# Copy preset files onto server forcefully
echo "Applying preset: $PRESET_ROOT"
cd preset
cp --recursive --force ./* $GMOD_ROOT
COPY_SUCCESS=$!
cd "$PRESET_ROOT"

# Replace template variables
echo "sed'ing variables."
SERVER_CFG="${GMOD_ROOT}cfg/server.cfg"
cat $SERVER_CFG | sed -e 's/{{/${/g' -e 's/}}/}/g')) > $SERVER_CFG

echo "Preset applied!"
exit 0