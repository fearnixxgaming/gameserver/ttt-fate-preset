; These settings describe the default configuration and text to be shown on the MOTD. This only applies if ulx showMotd is set to 1.
; All style configuration is set, and the values must be valid CSS.
; Under info, you may have as many sections as you like. Valid types include "text", "ordered_list", "list".
; Special type "mods" will automatically list workshop and regular addons in an unordered list.
; Special type "admins" will automatically list all users within the groups specified in contents.
; For an example of all of these items, please see the default file generated in ulx\lua\data.lua

"info"	
{
	
	{
		"type"	"text"
		"title"	"Paar Infos:"
		"contents"	
		{
			"Allgemeines"
			""
			"TTT Fate"
			"TTT Fate ist eine Abwandlung vom Standard trouble in terrorist town. Dabei erhält jeder Spieler zu seiner Rolle, eine Klasse. Am Anfang der runde wählt der Spieler sich zwischen 2 Klassen eine aus und erhält 2 neue Items. Es können auch Traitor Waffen dabei sein was es schwieriger macht ihn zu Identifizieren. Außerdem sollen die Rollen das Spiel erweitern und selbst für erfahrene Spieler schwerer machen."
			""
			"Im Chat, Adminchat oder Voicechat zu spammen ist strengstens untersagt!"
			"Andere Spieler zu beleidigen, zu diskriminieren, runterzumachen, zu belästigen oder zu \"trollen\" ist verboten."
			"Bugabuse, d.h das Ausnutzen von (Programmier-)Fehlern, ist verboten."
			"Propclimb/surf ist verboten. Darunter versteht man das unrealistische Klettern oder Surfen auf Props."
			"RDM, also grundloses Töten von anderen Spielern ist verboten."
			"Begründet ist das Töten eines anderen Spielers nur durch die eigene Rolle (Traitor), einen durch Beweise begründeten Traitor-Verdacht, das Befolgen von KoS-Calls oder eine Anweisung von Detektiven."
			"Das grundlose Töten von Spielern, welche AFK sind, ist verboten. Diese werden von selbst in den Zuschauermodus versetzt."
			"Es ist verboten, Informationen als nicht beteiligter oder bereits verstorbener Spieler an lebende Spieler weiterzugeben (sprich: nach dem Tod - FUNKSTILLE). Ebenso ist es untersagt, spielentscheidende Informationen (Rollen verraten / KoS setzen) über andere Wege an lebendige Spieler zu kommunizieren. Die Verwendung fälschlich erhaltener Informationen ist zu vermeiden."
			"Die Runde unnötig heraus zu zögern ist verboten, wie z.B. bei extensivem Camping als Traitor."
			"Spielt halbwegs richtig! Als Traitor und als Innocent/Detektiv."
			"Bei Traitor “One-Shot” Waffen (zb. Shooterking/Snowball/Caddy Lake) mit Verzögerung darf der Spieler jemanden KoS setzen, solange er noch nicht tot ist."
			"Auf unseren TTT-Server herrscht TS-Pflicht."
			"“Camping” ist untersagt. Ein Detektiv kann es jedoch anordnen.."
			"Um jemanden KoS (Kill on Sight) zu setzen, werden Indizien benötigt:"
			"Beschuss anderer Spieler oder dir selbst"
			"Traitor-Markierung im Scoreboard"
			"Spieler einer Lebensgefahr aussetzen (wie z.B. gezündete Granaten auf Spieler werfen oder Fallen stellen/aktivieren)"
			"Prop-Kill/Push"
			"Befehle des Detektivs mehrfach/eindeutig ignorieren"
			"Durchgängige auffällige evtl. bewaffnete Verfolgung (1 Mal muss vorher vorgewarnt werden)"
			"Durchgänge von Spielern grundlegend blockieren (3 Mal vorher vorgewarnt werden, eine Vorwarnung jede volle Sekunde erlaubt)"
			""
			""
			""
			""
			""
			"TTT Rollen"
			""
			"Innocent"
			"Als Innocent versuchst du mit Hilfe des Detektivs am Leben zu bleiben und die Traitor zu Identifizieren."
			""
			"Die bewusste Kooperation als Innocent mit einem Traitor ist immer verboten!"
			"Das grundlose Verletzen/Töten eines Spielers mit Granaten ist verboten. Es ist auch untersagt, sich selbst mit der Granate eines anderen zu töten, um diesem zu schaden."
			"Behinderung von Detektiven ist zu unterlassen. Ihren Anweisungen sollte Folge geleistet werden."
			"Das mutwillige Zerstören von platzierbaren Gegenständen des Detektivs ist nicht erlaubt."
			"Es darf nur mit begründbaren Verdacht getötet werden."
			""
			""
			"Traitor"
			"Die Traitor müssen die Innocents und den Detektiv töten, bevor die Zeit abläuft. Mit “C” öffnest du den Traitor “Shop”, in dem du Vorteile für Punkte kaufen kannst. Für jeden Kill erhältst du einen weiteren Punkt und für einen Detektiv Kill sogar 2 Punkte. Jeden Kill den man während einer laufenden Runde macht, bekommen die Traitor 30 sekunden “Over Time” zugerechnet."
			""
			"Die nicht vorgegebene Kooperation als Traitor mit einem Innocent/Detektiv ist verboten!"
			"Das mutwillige/unbegründete Zerstören von platzierbaren Gegenständen anderer Traitor ist nicht erlaubt."
			""
			"Detektiv"
			"Der Detektiv ist von beginn an der Runde als “Safe” angesehn und soll die Traitor entlarven. Mit “C” öffnest du den Detektiv “Shop”, in dem du Vorteile für Punkte kaufen kannst. Für jeden Traitor Kill erhältst du einen weiteren Punkt."
			""
			"Die willentliche Kooperation als Detektiv mit einem Traitor ist verboten!"
			"Detektive dürfen Regeln aufstellen, jedoch nur um Innocents und andere Detektive zu schützen und/oder Traitor zu entlarven, wie z.B Test-Pflicht."
			"Das mutwillige/unbegründete Zerstören von platzierbaren Gegenständen anderer Detektive ist nicht erlaubt."
			"Es darf nur mit begründbaren Verdacht getötet werden."
			""
		}
	}
	
	{
		"type"	"text"
		"title"	"Melden von Problemen etc."
		"contents"	
		{
			"Schreibt ihr eine Chatnachricht mit einem @ davor so können nur Teammitglieder diese Nachricht sehen"
			""
			"Sollte keiner da sein könnt ihr auf unserem Ts"
			""
			"ts.fearnixx.de"
			""
			"einfach bescheid"
		}
	}
	"description"	"Willkommen bei Fearnixx"
}
"style"	
{
	"borders"	
	{
		"border_color"	"#000000"
		"border_thickness"	"2px"
	}
	"colors"	
	{
		"header_text_color"	"#ffff00"
		"header_color"	"#6d0101"
		"text_color"	"#ffff00"
		"background_color"	"#000000"
		"section_text_color"	"#ffff00"
	}
	"fonts"	
	{
		"server_name"	
		{
			"weight"	"normal"
			"family"	"Fraklin Gothic Medium"
			"size"	"32px"
		}
		"regular"	
		{
			"weight"	"normal"
			"family"	"Tahoma"
			"size"	"12px"
		}
		"subtitle"	
		{
			"weight"	"normal"
			"family"	"Fraklin Gothic Medium"
			"size"	"20px"
		}
		"section_title"	
		{
			"weight"	"normal"
			"family"	"Fraklin Gothic Medium"
			"size"	"26px"
		}
	}
}
